package main

import (
	"mime"
	"os"
	"path/filepath"
)

func absPathMime(path string) string {
	ext := filepath.Ext(path)
	return mime.TypeByExtension(ext)
}

func isFile(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}

	return fileInfo.Mode().IsRegular()
}
