# Shavit

Shavit is a configurable Gemini server for UNIX operating systems. The server
is in a very early state and can only serve static files but in the near future
it will support dynamically generated files like a search endpoints and an Atom
feed.

## Building

The server is written in Go so to build it you only need to run one command:

    go build

This will produce a binary named `shavit` that you can run.

## Running

After you have compiled the server you need to configure it before it will run.
The server expect a configuration file in `/etc/shavit/config.toml` that contain
a path to the documents directory and the certificate files. A simple
configuration file might look like this:

    source = "/var/gemini/docs"
    tls_certificate = "/var/gemini/server.crt"
    tls_key = "/var/gemini/server.key"

With this configuration file the server will look for a certificate and key in
`/var/gemini` and for documents in `/var/gemini/docs`.

## Fork

This is a fork by Tom Ryder <tom@sanctum.geek.nz> that replaces the default
listening socket with systemd activation.
