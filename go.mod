module sanctum.geek.nz/code/shavit.git.git

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/pelletier/go-toml v1.6.0 // indirect
	sanctum.geek.nz/code/go-gemini.git.git v0.0.0-tejr1
)
