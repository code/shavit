package main

import (
	"log"

	gemini "sanctum.geek.nz/code/go-gemini.git.git"
)

// LoggingHandler wrap a Gemini handler and log all the requsts and responses
type LoggingHandler struct {
	handler gemini.Handler
}

// Handle implement the gemini.Handler interface by logging each request and response
func (h LoggingHandler) Handle(req gemini.Request) gemini.Response {
	log.Println("Received request for", req.URL)

	res := h.handler.Handle(req)
	log.Println("Responsed for", req.URL, "with ", res.Status, res.Meta)

	return res
}
