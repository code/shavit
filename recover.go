package main

import (
	gemini "sanctum.geek.nz/code/go-gemini.git.git"
)

// RecovererHandler wrap a Gemini handler and recover from panics
type RecovererHandler struct {
	handler gemini.Handler
}

// Handle implement the gemini.Handler interface by recovering inner handler
func (h RecovererHandler) Handle(req gemini.Request) (res gemini.Response) {
	defer func() {
		if r := recover(); r != nil {
			res = gemini.Response{Status: gemini.StatusTemporaryFailure, Meta: "Internal server error", Body: nil}
		}
	}()
	return h.handler.Handle(req)
}
